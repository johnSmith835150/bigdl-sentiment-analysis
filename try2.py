"""
reated on Sun Jan  6 12:09:28 2019
@author: Muhammed Buyukkinaci
"""
import re
import os
import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem.porter import *
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.text import text_to_word_sequence
from keras.preprocessing.sequence import pad_sequences
def stem(x):
    stemmer = PorterStemmer()
    words = x.split()
    stemmedWords = [stemmer.stem(word) for word in words]
    return ' '.join(stemmedWords)
def remove_stop_words(x):
    stop_words = set(stopwords.words('english'))
    processed_words = []
    words = x.split()
    for word in words:
        if word not in stop_words:
            processed_words.append(word)
    return ' '.join(processed_words)
def remove_punctuations(x):
    punctuations = ["@", "[", "]", "(", ")", ",", ".", "/", "_", "-", ":", "?"]
    for punctuation in punctuations:
        x = x.replace(punctuation, "")
    return x
def remove_numbers(x):
    return ''.join([i for i in x if not i.isdigit()])
np.random.seed(51)
MAX_WORD_TO_USE = 100000
MAX_LEN = 80
train = pd.read_csv('train_2.csv')

Y = pd.get_dummies(train['sentiment']).values
print(Y)
print(type(Y))
train['text'] = train['text'].apply(lambda x: ' '.join(text_to_word_sequence(x)))
train['text'] = train['text'].apply(lambda x: remove_stop_words(x))
train['text'] = train['text'].apply(lambda x: remove_punctuations(x))
train['text'] = train['text'].apply(lambda x: remove_numbers(x))
train['text'] = train['text'].apply(lambda x: stem(x))
# Tokenize the sentences
tokenizer = Tokenizer(nb_words=MAX_WORD_TO_USE)
textList = [str(sentence) for sentence in list(train['text'])]
tokenizer.fit_on_texts(textList)
train_X = tokenizer.texts_to_sequences(textList)
train_X = pad_sequences(train_X, maxlen=MAX_LEN)
words_dict = tokenizer.word_index
#print(words_dict)
print(type(train_X))

