from __future__ import print_function
import numpy as np 
from collections import Counter, OrderedDict
import re
import json

from pyspark.sql import SparkSession, Row
from pyspark.sql.functions import *
from pyspark.sql.functions import udf
from pyspark.ml.feature import HashingTF, Tokenizer
from pyspark import SparkContext

from pyspark.streaming import StreamingContext
from pyspark.sql.types import StructType, StructField, StringType
from pyspark.streaming.kafka import KafkaUtils
from pyspark.sql import SQLContext , DataFrame

from bigdl.nn.keras.layer import *
from bigdl.nn.keras.topology import Sequential
from bigdl.nn.criterion import *
from bigdl.util.common import *
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords

sc=SparkContext.getOrCreate(conf=create_spark_conf().setMaster("yarn").set("spark.driver.memory","10g"))

ssc = StreamingContext(sc, 1)
sqlContext = SQLContext(sc)

def getFeatures(rdd):
    if rdd.isEmpty():
      return

    parsed = rdd.map(lambda w: json.loads(w[1]))
    data = parsed.map(lambda e: Row(text=e["preprocessedText"], sentiment = str(e["sentiment"])))
    
    #return data
    print(data)
"""
    df = sqlContext.createDataFrame(data)
    df.printSchema()
    features  = df.select('text').collect()
    labels = df.select('sentiment').collect()
    for feature in features: 
        print(type(feature))
        print(feature)
        print(np.array(feature))
        print("HELLLOOOOOOOOOOOOOO")
"""

init_engine()
show_bigdl_info_logs()

topic = "result-processed"
brokers = "192.168.23.140:9092,192.168.23.141:9092,192.168.23.142:9092"

#sc = SparkContext(appName="PythonSparkStreamingDemo")
kafkaParams = {"metadata.broker.list": brokers, "auto.offset.reset": "smallest"}
kafkaDStream = KafkaUtils.createDirectStream(ssc, [topic], kafkaParams)

print(type(kafkaDStream))
#kafkaDStream.printSchema()

kafkaDStream.foreachRDD(getFeatures)



#df.printSchema()
#features  = df.select('text').collect()

ssc.start()
ssc.awaitTermination()




spark = SparkSession.builder.master("yarn").appName('LSTM').getOrCreate()


max_fatures = 2000

sent_list = data.select('sentiment').collect()
my_sent = []


Y=[1 if label =='Positive' else 0 for label in my_sent]
Y = np.array(Y)


# construct model architecture 

embed_dim = 128
lstm_out = 196

model = Sequential()
model.add(Embedding(max_fatures, embed_dim,input_shape = (X.shape[1],)))
model.add(SpatialDropout1D(0.4))
model.add(LSTM(lstm_out))
model.add(Dense(1,activation='softmax'))
model.compile(loss = 'categorical_crossentropy', optimizer='adam',metrics = ['accuracy'])

msize = len(X)
X_train = X[:int(msize*0.8)]
Y_train = Y[:int(msize*0.8)]
X_test = X[int(msize*0.8):]
Y_test = Y[int(msize*0.8):]

print(X.shape,Y.shape)
print(X.shape,Y.shape)


batch_size = 32

print(model.get_input_shape()) # (None, 28)
print(model.get_output_shape()) # (None, 1)

train_data = to_sample_rdd(X_train, Y_train)
test_data = to_sample_rdd(X_test, Y_test)

model.fit(train_data, nb_epoch = 20,distributed=True, batch_size = batch_size)#,validation_data=test_data)

result=model.evaluate(test_data, batch_size = batch_size)
print(result[0])

model.saveModel("hdfs:///user/root/LSTMmodel.bigdl", "hdfs:///user/root/LSTMmodel.bin", True) 
