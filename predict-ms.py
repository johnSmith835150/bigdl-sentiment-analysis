from __future__ import print_function
import numpy as np 
from collections import Counter
import re
import json

from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.functions import udf
from pyspark.ml.feature import HashingTF, Tokenizer
from pyspark import SparkContext
from bigdl.nn.layer import *
from bigdl.optim.optimizer import *

from bigdl.nn.keras.layer import *
from bigdl.nn.keras.topology import Sequential
from bigdl.nn.criterion import *
from bigdl.util.common import *
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords

def preprocessing(data, spark):
    data = data[['text','sentiment']]
    
    data = data[data.sentiment != "Neutral"]
    data = data.filter(data.text.isNotNull())
    data = data.filter(data.sentiment.isNotNull())
    data.show(3)
    clean_text_udf = udf(clean_text)
    data = data.withColumn("text" , clean_text_udf("text"))
    data.show(5)

    print(data[ data['sentiment'] == 'Positive'].count())
    print(data[ data['sentiment'] == 'Negative'].count())

    tokenizer = Tokenizer(inputCol="text", outputCol="vector")
    data = tokenizer.transform(data)
    data.show(5)

    text_list = data.select('vector').collect()

    stemmer = SnowballStemmer("english")
    stop_words = set(stopwords.words('english')) 

    my_text = []
    for row in text_list:
        for texts in row:
            #for text in texts:
            stemmedText = stemmer.stem(str(texts))
            if (not stemmedText in stop_words):
                my_text.append(stemmedText)

    # load word dict
    wordDictRDD = spark.read.json("hdfs:///user/root/wordDict2.json")
    wordDict = json.loads(wordDictRDD.toJSON().collect()[0])
    print(type(wordDict))
    print("TYPEEEEEEE")
    print(type(my_text[0]))
    text_int = []
    for text in my_text:
        r=[]
        for w in text.split():
        #    print("outside if ")
            if w in wordDict:
                #print("inside if "+w)
                r.append(wordDict[w])
        text_int.append(r)
        #r = [wordDict[w] if w in wordDict for w in text.split()]
        #text_int.append(r)
    print("Printing text int")
    print (text_int[0:3])

    k = 100 # MAX NO OF WORDS  
    return pad_features(text_int, k)

def pad_features(text_int, seq_length):
    ''' Return features of text_ints, where each tweet is padded with 0's or truncated to the input seq_length.
    '''
    features = np.zeros((len(text_int), seq_length), dtype = int)
    
    for i, tweet in enumerate(text_int):
        tweet_len = len(tweet)
        
        if tweet_len <= seq_length:
            zeroes = list(np.zeros(seq_length-tweet_len))
            new = zeroes+tweet
        elif tweet_len > seq_length:
            new = tweet[0:seq_length]
        
        features[i,:] = np.array(new)
    
    return features


def clean_text(c):
  c = c.lower()
  c = re.sub('[^a-zA-z0-9\s]','',c)
  c = c.replace('rt',' ')
  return c

init_engine()

spark = SparkSession.builder.master("yarn").appName('LSTM').getOrCreate()
data = spark.read.load('Sentiment.csv', format ="csv" ,header='true', inferschema='true')
#(tmrommromrgmaxirain_set, val_set, test_set) = data.randomSplit([0.01, 0.2, 0.5], seed = 2000)
#data = train_set

model = Model.loadModel("hdfs:///user/root/LSTMmodel2.bigdl", "hdfs:///user/root/LSTMmodel2.bin")

sent_list = data.select('sentiment').collect()
my_sent = []
for i in range(len(sent_list)):
    m = str (sent_list[i].sentiment)
    my_sent.append(m)

X_test = preprocessing(data, spark)
Y_test = [0 if label =='Positive' else 1 for label in my_sent]
Y_test = np.array(Y_test)

result = model.predict(X_test, distributed=True)
results = result.collect()
print(len(Y_test), ", ", len(results))
tr = 0
fl = 0
for i in range(len(results)):
    if np.argmax( results[i]) == Y_test[i]:
        tr +=1
    else:
        fl += 1
    print("result: ",np.argmax( results[i]), ", expected: ", Y_test[i])
print (str(tr))
print(str(fl))
