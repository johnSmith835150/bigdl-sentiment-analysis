from __future__ import print_function
import numpy as np
from collections import Counter
import re
import json

from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.functions import udf, monotonically_increasing_id
from pyspark.ml.feature import HashingTF, Tokenizer
from pyspark import SparkContext

from pyspark.streaming import StreamingContext
from pyspark.sql.types import StructType, StructField, StringType
from pyspark.streaming.kafka import KafkaUtils
from pyspark.sql import SQLContext, DataFrame, Row
from pyspark.sql.types import StringType

from bigdl.nn.layer import *
from bigdl.optim.optimizer import *
from bigdl.nn.keras.layer import *
from bigdl.nn.keras.topology import Sequential
from bigdl.nn.criterion import *
from bigdl.util.common import *
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers=['192.168.23.140:9092', '192.168.23.141:9092',
                                            '192.168.23.142:9092'], value_serializer=lambda x: json.dumps(x).encode('utf-8'))

sc = SparkContext.getOrCreate(conf=create_spark_conf().setMaster(
    "yarn").set("spark.driver.memory", "10g"))
ssc = StreamingContext(sc, 1)
sqlContext = SQLContext(sc)

#model = Model.loadModel("hdfs:///user/root/LSTMmodelBatch768_F.bigdl",
#                        "hdfs:///user/root/LSTMmodelBatch768_F.bin")
model = Model.loadModel("hdfs:///user/root/LSTMAmazonModel.bigdl","hdfs:///user/root/LSTMAmazonModel.bin")


def printValues(record):
    print(record)
def predictSentiment(rdd):
    if rdd.isEmpty():
        return

    parsed = rdd.map(lambda w: json.loads(w[1]))
    data = parsed.map(lambda e: Row(text=str(e["text"]), user=str(e["user"]), language=str(
        e["language"]), timestamp=str(e["timestamp"]), preprocessedText=e["preprocessedText"],noOfWords = str(e["noOfWords"])))

    df = sqlContext.createDataFrame(data)
    df.printSchema()

    features = df.select('preprocessedText').collect()
    
    print("An RDD")
    X = []
    for feature in features:
        m = np.array(feature[0])
        print(m.shape)
        X.append(m)

    X = np.array(X)

    #print(X)
    print(type(X))

    result = model.predict(X, distributed=True)
    print(result)
    print("PRINTTINNNG FOR DEBUGGING")
    #result.foreach(lambda x: print(x))
    results = result.collect()
    predictionsLabels = ["neg", "pos"]
    predictions = []
    for i in range(len(results)):
        predictions.append(predictionsLabels[np.argmax(results[i])])

    predictionsDF = sqlContext.createDataFrame(predictions, StringType())
    predictionsDF = predictionsDF.withColumnRenamed("value", "sentiment")
    predictionsDF.show()

    dfzip= df.rdd.zipWithIndex()
    # return back to dataframe
    dfzip = dfzip.toDF()
    dfzip.show()
    

    predictionsDFzip = predictionsDF.rdd.zipWithIndex()
    # return back to dataframe
    predictionsDFzip = predictionsDFzip.toDF()
    predictionsDFzip.show()

    
    df11 = df.select("*").withColumn("columnindex",
                                     monotonically_increasing_id())
    #df22 = predictionsDF.select(
    #    "*").withColumn("columnindex", monotonically_increasing_id())
    df11.show(5)
    #df22.show(5)

    joined = df11.join(dfzip, df11.columnindex ==
                       dfzip._2, 'inner').drop(dfzip._2)
    joined = joined.drop(joined._1)
    joined.show(5)
    joined.printSchema()
    joined2 = joined.join(predictionsDFzip, joined.columnindex == predictionsDFzip._2,'inner').drop(predictionsDFzip._2)
    joined2 = joined2.drop(joined2.columnindex)
    joined2.show()
    list_elements = joined2.collect()
    messages = []
    print("REEEM")
    print(len(list_elements))
    print(list_elements)
    for element in list_elements:
        message = {
            'language': str(element[0]),
            'noOfWords':str(element[1]),
            'preprocessedText': eval(str(element[2])),
            'text': str(element[3]),
            'timestamp': str(element[4]),
            'user': str(element[5]),
            'sentiment': str(element[6].asDict().values()[0])
        }
        messages.append(message)
    for message in messages:
        producer.send("slack-to-hive",value=message)
init_engine()
show_bigdl_info_logs()

topic = "slack-output"
brokers = "192.168.23.140:9092,192.168.23.141:9092,192.168.23.142:9092"

kafkaParams = {"metadata.broker.list": brokers,
               "auto.offset.reset": "smallest"}
kafkaDStream = KafkaUtils.createDirectStream(ssc, [topic], kafkaParams)


kafkaDStream.foreachRDD(predictSentiment)


ssc.start()
ssc.awaitTermination()
