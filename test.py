from __future__ import print_function
import numpy as np 
from collections import Counter, OrderedDict
import re
import json

from pyspark.sql import SparkSession, Row
from pyspark.sql.functions import *
from pyspark.sql.functions import udf
from pyspark.ml.feature import HashingTF, Tokenizer
from pyspark import SparkContext

from bigdl.nn.keras.layer import *
from bigdl.nn.keras.topology import Sequential
from bigdl.nn.criterion import *
from bigdl.util.common import *
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords

import time

# sc=SparkContext.getOrCreate(conf=create_spark_conf().setMaster("yarn").set("spark.driver.memory","10g"))
# show_bigdl_info_logs()
init_engine()
show_bigdl_info_logs()
def pad_features(text_int, seq_length):
    ''' Return features of text_ints, where each tweet is padded with 0's or truncated to the input seq_length.
    '''
    features = np.zeros((len(text_int), seq_length), dtype = int)
    
    for i, tweet in enumerate(text_int):
        tweet_len = len(tweet)
        
        if tweet_len <= seq_length:
            zeroes = list(np.zeros(seq_length-tweet_len))
            new = zeroes+tweet
        elif tweet_len > seq_length:
            new = tweet[0:seq_length]
        
        features[i,:] = np.array(new)
    
    return features


def clean_text(c):
  c = c.lower()
  c = re.sub('[^a-zA-z0-9\s]','',c)
  c = c.replace('rt',' ')
  return c

spark = SparkSession.builder.master("yarn").appName('LSTM').getOrCreate()

data = spark.read.load('Sentiment.csv', format ="csv" ,header='true', inferschema='true')
#(train_set, val_set, test_set) = data.randomSplit([0.5, 0.1, 0.1], seed = 2000)
#data = train_set

max_fatures = 200000

# Keeping only the neccessary columns
data = data[['text','sentiment']]

#Sentiment = data ['sentiment']
#text = data['text']
data = data[data.sentiment != "Neutral"]
data = data.filter(data.text.isNotNull())
data = data.filter(data.sentiment.isNotNull())
data.show(3)
clean_text_udf = udf(clean_text)
data = data.withColumn("text" , clean_text_udf("text"))
data.show(5)

print(data[ data['sentiment'] == 'Positive'].count())
print(data[ data['sentiment'] == 'Negative'].count())

tokenizer = Tokenizer(inputCol="text", outputCol="vector")
data = tokenizer.transform(data)
data.show(5)

text_list = data.select('vector').collect()

stemmer = SnowballStemmer("english")
stop_words = set(stopwords.words('english')) 

### no hope in keras text to sequence ##
## so creating vocab to int mapping dictionary

my_text = []
for row in text_list:
    for texts in row:
       # for text in texts:
        stemmedText = stemmer.stem(str(texts))
        if (not stemmedText in stop_words):
            my_text.append(stemmedText)
#print(my_text)
all_text2 = ' '.join(my_text)
words = all_text2.split()
count_words = Counter(words)

total_words = len(words)
sorted_words = count_words.most_common(total_words)

vocab_to_int = {w:i+1 for i, (w,c) in enumerate(sorted_words)}

def convert_to_row(d):
    return Row(**OrderedDict(sorted(d.items())))

#word_dict_df = spark.sparkContext.parallelize([vocab_to_int]).map(convert_to_row).toDF()
#word_dict_df.write.format('json').save("hdfs:///user/root/word_dict2.json")
#exit()
# print (vocab_to_int)

text_int = []
#k=0
for text in my_text:
   # if len(text.split())>k:
       # k =  len(text.split())
    r = [vocab_to_int[w] for w in text.split()]
    text_int.append(r)
print (text_int[0:3])

k = 100 # MAX NO OF WORDS  
X = pad_features(text_int,k)
print(type(X))
print(X[0:3])

sent_list = data.select('sentiment').collect()
my_sent = []
for i in range(len(sent_list)):
    m = str (sent_list[i].sentiment)
    my_sent.append(m)

Y=[[1,0] if label =='Positive' else [0,1] for label in my_sent]
Y = np.array(Y)

Y_for_test = [0 if label =='Positive' else 1 for label in my_sent]
Y_for_test = np.array(Y_for_test)

embed_dim = 128
lstm_out = 196

model = Sequential()
model.add(Embedding(max_fatures, embed_dim,input_shape = (X.shape[1],)))
model.add(SpatialDropout1D(0.2))
model.add(LSTM(lstm_out,return_sequences=True))
model.add(LSTM(lstm_out))
#model.add(LSTM(lstm_out, activation="tanh", inner_activation="hard_sigmoid", return_sequences=True))
#model.add(LSTM(96, activation="tanh", inner_activation="hard_sigmoid", return_sequences=True))
#model.add(Dense(64,activation='relu'))
# model.add(LSTM(lstm_out, dropout=0.2, recurrent_dropout=0.2))
model.add(Dense(2,activation='softmax'))
model.compile(loss = 'categorical_crossentropy', optimizer='adam',metrics = ['accuracy'])

msize = len(X)
X_train = X[:int(msize*0.8)]
Y_train = Y[:int(msize*0.8)]
X_test = X[int(msize*0.8):]
Y_test = Y[int(msize*0.8):]

#Y_new = [0 if label == [1 0] else 1 for label in Y_test]
Y_new = Y_for_test[int(msize*0.8):]

print("PRINTINNNNNNGGGGGG")
#print(type(X))
#print(X[0:5])

print(X.shape,Y.shape)
print(X.shape,Y.shape)


batch_size = 120 # batch size should be multiple of no od=f executers which 6 for now 

print(model.get_input_shape()) # (None, 28)
print(model.get_output_shape()) # (None, 1)


train_data = to_sample_rdd(X_train, Y_train)
#test_data = to_sample_rdd(X_test, Y_test)
#test_data = to_sample_rdd(X_text,Y_new)

#for row in test_data.collect():
#    print(row)


model.fit(train_data, nb_epoch =10,distributed=True, batch_size = batch_size)#,validation_data=test_data)

result = model.predict(X_test, distributed=True)
results = result.collect()
print(len(Y_test), ", ", len(results))
tr = 0
fl = 0

#predictions =[]
#actual =[]
for i in range(len(results)):
    #predictions.append(np.argmax( results[i]) )
    #actual.append(Y_test[i])
    print(results[i][0]," ", results[i][1])
    print("result: ",np.argmax( results[i]), ", expected: ", Y_new[i])
    if np.argmax( results[i]) == Y_new[i]:
        tr +=1
    else:
        fl += 1
    print(results[i][0]," ", results[i][1])
    print("result: ",np.argmax( results[i]), ", expected: ", Y_new[i])
print (str(tr))
print(str(fl))

acc = str(float(tr)/len(results))

tim = str(time.time())

modelFile = "hdfs:///user/root/LSTMmodel2_"+tim+"Accuracy_"+acc+".bigdl"
modelFileWeights = "hdfs:///user/root/LSTMmodel2_"+tim+"Accuracy_"+acc+".bin"

model.saveModel(modelFile,modelFileWeights,True)

#result=model.evaluate(test_data, batch_size = batch_size)
#print(result[0])

#model.saveModel("hdfs:///user/root/LSTMmodel3.bigdl", "hdfs:///user/root/LSTMmodel3.bin", True) 
