import re
import os
import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.text import text_to_word_sequence
from keras.preprocessing.sequence import pad_sequences

def lemmatize(x):
    lemmatizer = WordNetLemmatizer()
    words = x.split()
    words = [lemmatizer.lemmatize(word) for word in words]
    return ' '.join(words)

def remove_stop_words(x):
    stop_words = set(stopwords.words('english'))
    processed_words = []

    words = x.split()
    for word in words:
        if word not in stop_words:
            processed_words.append(word)

    return ' '.join(processed_words)

def remove_punctuations(x):
    punctuations = ["@", "[", "]", "(", ")", ",", ".", "/", "_", "-", ":", "?"]
    for punctuation in punctuations:
        x = x.replace(punctuation, "")

    return x

def remove_numbers(x):
    x = re.sub('[0-9]{5,}', '#####', x)
    x = re.sub('[0-9]{4}', '####', x)
    x = re.sub('[0-9]{3}', '###', x)
    x = re.sub('[0-9]{2}', '##', x)
    return x

np.random.seed(51)
MAX_WORD_TO_USE = 100000
MAX_LEN = 80

train = pd.read_csv('input.csv')

train['text'] = train['text'].apply(lambda x: ' '.join(text_to_word_sequence(x)))
train['text'] = train['text'].apply(lambda x: lemmatize(x))
train['text'] = train['text'].apply(lambda x: remove_stop_words(x))
train['text'] = train['text'].apply(lambda x: remove_punctuations(x))
train['text'] = train['text'].apply(lambda x: remove_numbers(x))

# Tokenize the sentences
tokenizer = Tokenizer(nb_words=MAX_WORD_TO_USE)
textList = [ str(sentence) for sentence in list(train['text'])]
tokenizer.fit_on_texts(textList)

train_X = tokenizer.texts_to_sequences(textList)
train_X = pad_sequences(train_X, maxlen=MAX_LEN)

words_dict = tokenizer.word_index
print(words_dict)
