#!/bin/bash

uage()
{
    echo "usage: bigdl [[[-f | --file python file to excute ] | [-h | --help]]"
}

file=""

while [ "$1" != "" ]; do
    case $1 in
        -f | --file )           shift
                                file=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ "$file" = "" ]; then
	echo "Error: file name must be provided"
    exit
fi

if [[ ! -f $file ]]; then
    echo "$file is not a valid python file"
    exit 1
fi

case $file in
  /*) file=$file ;;
  *) file="${PWD}/${file}" ;;
esac

BigDL_HOME=/opt/bigDl
SPARK_HOME="/opt/cloudera/parcels/CDH-6.2.0-1.cdh6.2.0.p0.967373/lib/spark"
PYTHON_API_PATH=${BigDL_HOME}/lib/bigdl-0.9.0-python-api.zip
BigDL_JAR_PATH=${BigDL_HOME}/lib/bigdl-SPARK_2.4-0.9.0-jar-with-dependencies.jar
PYTHONPATH=${PYTHON_API_PATH}:$PYTHONPATH
VENV_HOME=/opt/bigDl/bin
SPARK_STREAM_JAR_PATH=${PWD}/spark-streaming-kafka-0-8-assembly_2.11-2.4.3.jar

PYSPARK_PYTHON=/opt/bigDl/bin/venv-compressed/venv/bin/python ${SPARK_HOME}/bin/spark-submit \
--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=/opt/bigDl/bin/venv-compressed/venv/bin/python \
--master yarn-client \
--executor-memory 7g \
--driver-memory 7g \
--executor-cores 2 \
--num-executors 6 \
--properties-file ${BigDL_HOME}/conf/spark-bigdl.conf \
--jars ${BigDL_JAR_PATH},${SPARK_STREAM_JAR_PATH}  \
--py-files ${PYTHON_API_PATH} \
--archives ${VENV_HOME}/venv-compressed \
--conf spark.driver.extraClassPath=bigdl-SPARK_2.4-0.9.0-jar-with-dependencies.jar \
--conf spark.executor.extraClassPath=bigdl-SPARK_2.4-0.9.0-jar-with-dependencies.jar \
${file}
